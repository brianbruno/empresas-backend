# Empresas - Backend

### O QUE FOI FEITO?

- Fork do repositório https://bitbucket.org/ioasys/empresas-backend/src/master/
- Link do repositório enviado a equipe ioasys

### ESCOPO DO PROJETO

- Deve ser criada uma API em **NodeJS** ou **Ruby on Rails**.

- A API deve fazer o seguinte:

1. Login e acesso de Usuário já registrado;

Feito na rota: `{{dev_host}}/api/{{api_version}}/users/auth/sign_in`

2. Para o login usamos padrões **JWT** ou **OAuth 2.0**;

Foi utilizado o padrão JWT.

3. Listagem de Empresas

Feito na rota: `{{dev_host}}/api/{{api_version}}/enterprises`

4. Detalhamento de Empresas

Feito na rota: `{{dev_host}}/api/{{api_version}}/enterprises/{{id}}`

5. Filtro de Empresas por nome e tipo

Feito na rota: `{{dev_host}}/api/{{api_version}}/enterprises?name={{ nome }}&enterprise_type={{ tipo }}`

### Informações Importantes

- A API está funcionando exatamente da mesma forma conforme disponibilizada na collection do postman.

  - Para o login usei o padrão JWT

- O mesmo padrão foi seguido e as rotas utilizadas na API são as mesmas da collection do Postman.

- O teste foi completamente finalizado.

- O Banco de dados utilizado foi o MySQL

### Dados para Teste

Não foi possível acessar os dados de testes disponiblizados na especificação do teste.
Portanto, criei os campos conforme imaginei que seria.
Peço que considerem esse ponto durante a análise do teste.

### Rodando o sistema

Utilizei o Framework SailsJS para criação da API.
Para executar, execute os comandos a seguir dentro da pasta `empresas`:

Caso esteja rodando pela primeira vez, certifique-se de que tenha o nodejs instalado no servidor.
Após isso, execute o seguinte comando: 

`npm install`

Para ligar o servidor:

`sails lift`

### Servidor MYSQL

Para alterar o servidor MYSQL basta alterar o arquivo /config/datastores.js
O nome do banco de dados utilizado foi `empresas`.

Para rodar facilmente um servidor MySQL em Docker utilize o comando abaixo:

```
docker run --name mysql-empresas -p 3306:3306 -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=empresas -e MYSQL_USER=user -e MYSQL_PASSWORD=pass -d mysql:5.6
```
