module.exports = function (req, res, next) {
  let token;

  if (req.headers && req.headers['access-token']) {
    let parts = req.headers['access-token'].split(' ');
    if (parts.length === 2) {
      let scheme = parts[0];
      let credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        token = credentials;
      }
    } else {
      return res.json(401, {err: 'Format is Authorization: Bearer [token]'});
    }
  } else if (req.param('token')) {
    token = req.param('token');
    delete req.query.token;
  } else {
    return res.json(401, {err: 'No Authorization header was found'});
  }

  let jwt = require('jsonwebtoken');
  const fs = require('fs');
  let publicKey  = fs.readFileSync('./public.key', 'utf8');
  jwt.verify(token, publicKey, {algorithm: ['RS256']}, (err, decoded) => {
    if (err) {
      return res.status(500).send({ auth: false, message: 'Token inválido.' });
    }

    req.userId = decoded.id;

    let privateKey  = fs.readFileSync('./private.key', 'utf8');

    let token = jwt.sign({ id: decoded.id }, privateKey, {
      expiresIn: 300,
      algorithm:  'RS256' //SHA-256 hash signature
    });
    res.set('access-token', 'Bearer ' + token);

    next();
  });
};
