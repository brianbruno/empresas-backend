module.exports = {

  attributes: {

    email: {
      type: 'string',
      description: 'Email address of the user.',
      required: true,
      unique: true,
      isEmail: true,
      maxLength: 200,
      example: 'contact@brian.place'
    },

    password: {
      type: 'string',
      description: 'User password.',
      protect: true,
      example: '2$sao029-k89aaarh'
    },

    fullName: {
      type: 'string',
      required: true,
      description: 'Full user\'s name',
      maxLength: 120,
      example: 'Brian Bruno'
    },

  },


};
