module.exports = {

  attributes: {

    name: {
      type: 'string',
      description: 'Enterprise\'s name.',
      required: true,
      maxLength: 200,
      example: 'ioasys'
    },

    description: {
      type: 'string',
      description: 'What is the enterprise purpose.',
      protect: true,
      example: '2$sao029-k89aaarh'
    },

    enterprise_type: {
      type: 'number',
      required: true,
      description: 'Enterprise\'s type.',
      example: '1'
    },

  },


};
