module.exports = {


  friendlyName: 'Shows Enterprise',


  description: 'Shows an enterprise.',


  fn: async function () {
    const id = this.req.param('id');

    const enterprises =await Enterprise.find({ id: id});

    return {
      enterprise: enterprises,
    };



  }
};
