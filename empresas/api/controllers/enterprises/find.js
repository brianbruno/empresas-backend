module.exports = {


  friendlyName: 'Search Enterprise',


  description: 'Search an enterprise.',


  fn: async function () {
    let enterpriseTypes = this.req.param('enterprise_types');
    let name = this.req.param('name');
    let filter = {}

    if (enterpriseTypes)
      filter['enterprise_type'] = enterpriseTypes;
    else if (name)
      filter['name'] = name;

    const enterprises =await Enterprise.find(filter);

    return {
      enterprises: enterprises,
    };



  }
};
