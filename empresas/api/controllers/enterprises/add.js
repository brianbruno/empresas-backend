module.exports = {


  friendlyName: 'Create Enterprise',


  description: 'Creates an enterprise.',


  inputs: {

    name: {
      description: 'Enterprise\'s name.',
      type: 'string',
      required: true
    },

    description: {
      description: 'What is the enterprise purpose.',
      type: 'string',
      required: true
    },

    enterprise_type: {
      description: 'Enterprise\'s type.',
      type: 'string',
      required: true
    }

  },


  exits: {
    success: {
      description: 'The enterprise has been successfully created.',
    },
  },


  fn: async function (inputs, exits) {


    const enterprise = {
      name: inputs.name,
      description: inputs.description,
      enterprise_type: inputs.enterprise_type
    };

    await Enterprise.create(enterprise);

    return exits.success();


  }
};
