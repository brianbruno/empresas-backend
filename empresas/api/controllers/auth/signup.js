module.exports = {


  friendlyName: 'Sign Up',


  description: 'Sign Up using email and password.',


  inputs: {

    email: {
      description: 'The email address.',
      type: 'string',
      required: true
    },

    password: {
      description: 'The unencrypted password.',
      type: 'string',
      required: true
    },


    fullName: {
      description: 'User\'s full name.',
      type: 'string',
      required: true
    }

  },


  exits: {

    success: {
      description: 'The user has been successfully logged in.',
    },

    badCombo: {
      description: 'The provided email and password combination does not match any user in the database.',
      responseType: 'unauthorized'
    }

  },


  fn: async function (inputs) {
    var newEmailAddress = inputs.email.toLowerCase();

    // Build up data for the new user record and save it to the database.
    // (Also use `fetch` to retrieve the new ID so that we can use it below.)
    var newUserRecord = await User.create(_.extend({
      email: newEmailAddress,
      password: await sails.helpers.passwords.hashPassword(inputs.password),
      fullName: inputs.fullName,
    }, sails.config.custom.verifyEmailAddresses ? {
      emailProofToken: await sails.helpers.strings.random('url-friendly'),
      emailProofTokenExpiresAt: Date.now() + sails.config.custom.emailProofTokenTTL,
      emailStatus: 'unconfirmed'
    }:{}))
      .intercept('E_UNIQUE', 'emailAlreadyInUse')
      .intercept({name: 'UsageError'}, 'invalid')
      .fetch();

    // If billing feaures are enabled, save a new customer entry in the Stripe API.
    // Then persist the Stripe customer id in the database.
    if (sails.config.custom.enableBillingFeatures) {
      let stripeCustomerId = await sails.helpers.stripe.saveBillingInfo.with({
        email: newEmailAddress
      }).timeout(5000).retry();
      await User.updateOne(newUserRecord.id)
        .set({
          stripeCustomerId
        });
    }
  }
};
