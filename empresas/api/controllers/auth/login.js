module.exports = {


  friendlyName: 'Login',


  description: 'Log in using the provided email and password combination.',


  inputs: {

    email: {
      description: 'The email address.',
      type: 'string',
      required: true
    },

    password: {
      description: 'The unencrypted password.',
      type: 'string',
      required: true
    }

  },


  exits: {

    success: {
      description: 'The user has been successfully logged in.',
    },

    badCombo: {
      description: 'The provided email and password combination does not match any user in the database.',
      responseType: 'unauthorized'
    }

  },


  fn: async function (inputs) {
    let jwt = require('jsonwebtoken');
    const fs = require('fs');

    // Look up by the email address.
    let userRecord = await User.findOne({
      email: inputs.email.toLowerCase(),
    });

    if(!userRecord) {
      throw 'badCombo';
    }

    await sails.helpers.passwords.checkPassword(inputs.password, userRecord.password).intercept(() => { throw 'badCombo'; });

    let privateKey  = fs.readFileSync('./private.key', 'utf8');

    let token = jwt.sign({ id: userRecord.id }, privateKey, {
      expiresIn: 300,
      algorithm:  'RS256' //SHA-256 hash signature
    });
    this.res.set('access-token', 'Bearer ' + token);

    return {
      id: userRecord.id,
    };


  }
};
