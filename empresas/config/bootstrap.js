/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function() {

  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)
  // if (await User.count() > 0) {
  //   return;
  // }
  //
  let userRecord = await User.findOne({email: 'lucasrizel@ioasys.com.br',});
  if (!userRecord) {
    await User.createEach([
      { email: 'lucasrizel@ioasys.com.br', fullName: 'Lucas Rizel', password: await sails.helpers.passwords.hashPassword('12345678')},
    ]);
  }

  /**
   * Types:
   * [1, "Agro"]
     [2, "Aviation"]
     [3, "Biotech"]
     [4, "Eco"]
     [5, "Ecommerce"]
     [6, "Education"]
     [7, "Fashion"]
     [8, "Fintech"]
     [9, "Food"]
     [10, "Games"]
     [11, "Health"]
     [12, "IOT"]
     [13, "Logistics"]
     [14, "Media"]
     [15, "Mining"]
     [16, "Products"]
     [17, "Real Estate"]
     [18, "Service"]
     [19, "Smart City"]
     [20, "Social"]
     [21, "Software"]
     [22, "Technology"]
     [23, "Tourism"]
     [24, "Transport"]
   */

  let enterprises = await Enterprise.count();
  if (enterprises === 0) {
    await Enterprise.createEach([
      { name: 'The Best Agro', description: 'Agro enterprise. The best Agro enterprise!', enterprise_type: '1'},
      { name: 'The Best Aviation', description: 'Aviation enterprise. The best Aviation enterprise!', enterprise_type: '2'},
      { name: 'The Best Biotech', description: 'Biotech enterprise. The best Biotech enterprise!', enterprise_type: '3'},
      { name: 'The Best Eco', description: 'Eco enterprise. The best Eco enterprise!', enterprise_type: '4'},
      { name: 'The Best Ecommerce', description: 'Ecommerce enterprise. The best Ecommerce enterprise!', enterprise_type: '5'},
      { name: 'The Best Education', description: 'Education enterprise. The best Education enterprise!', enterprise_type: '6'},
      { name: 'The Best Fashion', description: 'Fashion enterprise. The best Fashion enterprise!', enterprise_type: '7'},
      { name: 'The Best Fintech', description: 'Fintech enterprise. The best Fintech enterprise!', enterprise_type: '8'},
      { name: 'The Best Food', description: 'Food enterprise. The best Food enterprise!', enterprise_type: '9'},
      { name: 'The Best Games', description: 'Games enterprise. The best Games enterprise!', enterprise_type: '10'},
      { name: 'The Best Health', description: 'Health enterprise. The best Health enterprise!', enterprise_type: '11'},
      { name: 'The Best IOT', description: 'IOT enterprise. The best IOT enterprise!', enterprise_type: '12'},
      { name: 'The Best Logistics', description: 'Logistics enterprise. The best Logistics enterprise!', enterprise_type: '13'},
      { name: 'The Best Media', description: 'Media enterprise. The best Media enterprise!', enterprise_type: '14'},
      { name: 'The Best Mining', description: 'Mining enterprise. The best Mining enterprise!', enterprise_type: '15'},
      { name: 'The Best Products', description: 'Products enterprise. The best Products enterprise!', enterprise_type: '16'},
      { name: 'The Best Real ', description: 'Real  enterprise. The best Real  enterprise!', enterprise_type: '17'},
      { name: 'The Best Service', description: 'Service enterprise. The best Service enterprise!', enterprise_type: '18'},
      { name: 'The Best Smart ', description: 'Smart  enterprise. The best Smart  enterprise!', enterprise_type: '19'},
      { name: 'The Best Social', description: 'Social enterprise. The best Social enterprise!', enterprise_type: '20'},
      { name: 'The Best Software', description: 'Software enterprise. The best Software enterprise!', enterprise_type: '21'},
      { name: 'The Best Technology', description: 'Technology enterprise. The best Technology enterprise!', enterprise_type: '22'},
      { name: 'The Best Tourism', description: 'Tourism enterprise. The best Tourism enterprise!', enterprise_type: '23'},
      { name: 'The Best Transport', description: 'Transport enterprise. The best Transport enterprise!', enterprise_type: '24'},

    ]);
  }

  // ```

};
